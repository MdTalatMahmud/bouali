package com.workshop.bouali.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Employee {

    @Id
    @GeneratedValue
    private Integer id;
    private String identifier;
    private String firstname;
    private String lastname;
    private String email;
    private String birthdate;
    private String role;

}
